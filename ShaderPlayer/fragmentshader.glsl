

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv = fragCoord/iResolution.xy;
    vec2 p = vec2(uv.x, uv.y * (iResolution.y/iResolution.x));
    
    vec3 x = texture(iChannel0, uv).rgb;
    //x = 0.5*x + vec3(0.5);
    //x = pow(x, vec3(2.0))*4.0;
    //x = x*2.;
    fragColor = vec4(x, 1.0);
}
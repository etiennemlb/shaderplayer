#ifndef _FBOHandler
#define _FBOHandler

#include "shaderPlayerLogger.h"

#include <glad/glad.h>
#include <vector>

class FBOhandler
{
private:

	uint32_t mFBO;                   //framebuffer object
	uint32_t mTexture_color0;
	uint32_t mTexture_color1;

	uint32_t currentTexture;

	bool mIsFeedBackLoop;

	//	unsigned int mTexture_depth;
	GLenum mDrawbuffer[2];     //add texture attachements

	shaderPlayerLogger* mLogger;

public:

	explicit FBOhandler(shaderPlayerLogger* logger, const bool isFeedBackLoop) : mFBO(0), mTexture_color0(0),
		mTexture_color1(0), currentTexture(0),
		mIsFeedBackLoop(isFeedBackLoop),
		/*mTexture_depth(0),*/ mLogger(logger)
	{
	}

	~FBOhandler() {
		destroy();
	}

	//Generate FBO and two empty textures
	int32_t GenerateFBO(uint32_t width, uint32_t height);

	//return color texture from the framebuffer
	uint32_t getColorTexture();

	bool isFeedBackLoop();

	//return depth texture from the framebuffer
//	uint32_t getDepthTexture() const;

	//resize window
	void resize(uint32_t width, uint32_t height);

	//bind framebuffer to pipeline. We will  call this method in the render loop
	void bind() const;

	//unbind framebuffer from pipeline. We will call this method in the render loop
	void unbind() const;

private:

	//delete objects
	void destroy() const;

	//generate an empty color texture with 4 channels (RGBA8) using bilinear filtering
	void GenerateColorTexture(uint32_t &texture, uint32_t width, uint32_t height) const;


	//generate an empty depth texture with 1 depth channel using bilinear filtering
	//void GenerateDepthTexture(uint32_t width, uint32_t height);

};


#endif
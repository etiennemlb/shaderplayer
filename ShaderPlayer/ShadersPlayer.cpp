#include "ShadersPlayer.h"

shaderPlayerLogger ShadersPlayer::mLogger;
bool ShadersPlayer::mIsFullScreen;
bool ShadersPlayer::needFBOUpdate;

//################## START SHADER SRC ###############

const float ShadersPlayer::mVertices[VERTEXNUMBER(128)] = {
	-1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,

	-1.0f, 1.0f, 0.0f,
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
};

//############### Header ################

//			IMAGE
const char * ShadersPlayer::mSHeaderImage =
"#version 330 core\n"
"uniform vec3      iResolution;"                  // viewport resolution (in pixels)
"uniform float     iTime;"                        // shader playback time (in seconds)
"uniform float     iGlobalTime;"                  // shader playback time (in seconds)
"uniform vec4      iMouse;"                       // mouse pixel coords
"uniform vec4      iDate;"                        // (year, month, day, time in seconds)
"uniform float     iSampleRate;"                  // sound sample rate (i.e., 44100)
"uniform vec3      iChannelResolution[4];"        // channel resolution (in pixels)
"uniform float     iChannelTime[4];"              // channel playback time (in sec)
"uniform vec2      ifFragCoordOffsetUniform;"     // used for tiled based hq rendering
"uniform float     iTimeDelta;"                   // render time (in seconds)
"uniform int       iFrame;"                       // shader playback frame
"uniform float     iFrameRate;"

"struct Channel {"
"    vec3    resolution;"
"    float   time;"
"};"
"uniform Channel iChannel[4];"

/*
"uniform sampler2D	 iChannel0;"
"uniform samplerCube iChannel0;"
"uniform sampler3D	 iChannel0;"
 */

	"\n//######mSHeaderImage#####\n";

//			FBO (frame buffer object)
const char * ShadersPlayer::mSHeaderFBO =
"#version 330 core\n"
"uniform vec3      iResolution;"                  // viewport resolution (in pixels)
"uniform float     iTime;"                        // shader playback time (in seconds)
"uniform float     iGlobalTime;"                  // shader playback time (in seconds)
"uniform vec4      iMouse;"                       // mouse pixel coords
"uniform vec4      iDate;"                        // (year, month, day, time in seconds)
"uniform float     iSampleRate;"                  // sound sample rate (i.e., 44100)
"uniform vec3      iChannelResolution[4];"        // channel resolution (in pixels)
"uniform float     iChannelTime[4];"              // channel playback time (in sec)
"uniform vec2      ifFragCoordOffsetUniform;"     // used for tiled based hq rendering
"uniform float     iTimeDelta;"                   // render time (in seconds)
"uniform int       iFrame;"                       // shader playback frame
"uniform float     iFrameRate;"

"struct Channel {"
"    vec3    resolution;"
"    float   time;"
"};"
"uniform Channel iChannel[4];"

"\n//######mSHeaderFBO#####\n";

//			SOUND
const char * ShadersPlayer::mSHeaderSound =
"#version 330 core\n"
"uniform vec4      iDate;"                        // (year, month, day, time in seconds)
"uniform float     iSampleRate;"                  // sound sample rate (i.e., 44100)
"uniform vec3      iChannelResolution[4];"        // channel resolution (in pixels)
"uniform float     iChannelTime[4];"              // channel playback time (in sec)
"uniform float     iBlockOffset"

/* only this 2 type
"uniform sampler2D	 iChannel0;"
"uniform samplerCube iChannel0;"
*/

"\n//######mSHeaderSound#####\n";

//###################### DEFAULT #########################

const std::string ShadersPlayer::mChannelsLUT[3] = {
"sampler2D",
"sampler3D",
"samplerCube" };

/*for each vector given by vec3 aPos
*set the position(gl_position, each
*vectex created has one) of the newly created
*vertex with aPos.x, aPos.y, aPos.z
*layout(location = 0) means the index
*of the vertex attribute we want to pass in
*a vec3
* ie : 	float triangleVertices[] =
*			 {
-0.5f, -0.5f, 0.0f,
0.5f, -0.5f, 0.0f,
0.0f,  0.5f, 0.0f
};
*/
const char* ShadersPlayer::mDefaultVShader =
"#version 330 core\n"
"layout(location = 0) in vec3 vPos;\n"
"void main()\n"
"{\n"
"  gl_Position = vec4(vPos,1.0);\n"
"}\n";

const char* ShadersPlayer::mDefaultFShaderImage =
"void mainImage( out vec4 fragColor, in vec2 fragCoord )"
"{"
"vec2 uv = fragCoord/iResolution.xy;"
"vec3 col = 0.5 + 0.5*cos(iTime+uv.xyx+vec3(0,2,4));"
"fragColor = vec4(col,1.0);"
"}";

//			FBO
const char* ShadersPlayer::mDefaultFShaderFBO =
"void mainImage( out vec4 fragColor, in vec2 fragCoord )"
"{"
"vec2 uv = fragCoord/iResolution.xy;"
"vec3 col = 0.5 + 0.5*cos(iTime+uv.xyx+vec3(0,2,4));"
"fragColor = vec4(col,1.0);"
"}";

//			SOUND
const char* ShadersPlayer::mDefaultFShaderSound =
"vec2 mainSound( float time )"
"{"
"    // A 440 Hz wave that attenuates quickly overt time"
"    return vec2( sin(6.2831*440.0*time)*exp(-3.0*time) );"
"}";

//############### FOOTER ################


///			IMAGE
const char * ShadersPlayer::mSFooterImage =
"\n//######mSFooterImage#####\n"

"out vec4 glFragColor;"
"void main() {"
"  mainImage(glFragColor, gl_FragCoord.xy+ifFragCoordOffsetUniform);"
"  glFragColor.w = 1.0;"
"}";

//			FBO
const char * ShadersPlayer::mSFooterFBO =
"\n//######mSFooterFBO#####\n"

"out vec4 glFragColor;"
"void main() {"
"  mainImage(glFragColor, gl_FragCoord.xy+ifFragCoordOffsetUniform);"
//"  glFragColor.w = 1.0;"// if set, the alpha dimension will be deleted and some shaders dont like that
"}";

//			SOUND
const char* ShadersPlayer::mSFooterSound =
"\n//######mSFooterSound#####\n"

"layout(location = 1) out vec4 glFragColor;"
"void main()  {"
"float t = ifFragCoordOffsetUniform.x + (((iResolution.x-0.5+gl_FragCoord.x)/11025.) + (iResolution.y-.5-gl_FragCoord.y)*(iResolution.x/11025.));"
"vec2 y = mainSound( t );"
"vec2 v  = floor((0.5+0.5*y)*65536.0);"
"vec2 vl = mod(v,256.0)/255.0;"
"vec2 vh = floor(v/256.0)/255.0;"
"glFragColor = vec4(vl.x,vh.x,vl.y,vh.y);"
"}";

//################## END SHADER SRC ###############


int32_t ShadersPlayer::Init() const
{
	glfwSetErrorCallback(error_callback);

	if (!glfwInit()) {
		mLogger.Printf("glfwInit", "ShadersPlayer.cpp", "Init failed", LOG_ERROR);
		return -1;
	}

	mLogger.Printf("glfwInit", "ShadersPlayer.cpp", "Init successful", LOG_MESSAGE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

	return 0;
}

int32_t ShadersPlayer::createWindow(const uint32_t winWidth, const uint32_t winHeight, const char* const winTitle,
	GLFWmonitor* momnitor, GLFWwindow* share)
{
	if ((mWindow = glfwCreateWindow(winWidth, winHeight, winTitle, momnitor, share)) == nullptr)
	{
		mLogger.Printf("glfwCreateWindow", "ShadersPlayer.cpp", "Failed to create a window", LOG_ERROR);
		glfwTerminate();
		return -1;
	}

	mLogger.Printf("glfwCreateWindow", "ShadersPlayer.cpp", "Successful to create a window", LOG_MESSAGE);

	mWinWidth = winWidth;
	mWinHeight = winHeight;

	setKey_callback(key_callback);
	setFramebuffer_size_callback(framebuffer_size_callback);
	return 0;
}

GLFWwindow* ShadersPlayer::setContext(GLFWwindow * window) {
	if (window == nullptr)
		glfwMakeContextCurrent(mWindow);
	else
		glfwMakeContextCurrent(window);
	mLogger.Printf("setContext", "ShdersPlayer.cpp", "glfwMakeContextCurrent set", LOG_MESSAGE);

	setVSync(mVSync);
	mLogger.Printf("setContext", "ShdersPlayer.cpp", "glfwMakeContextCurrent set", LOG_MESSAGE);
	return window;
}

int32_t ShadersPlayer::initGlad()
{
	if (glfwGetCurrentContext() == nullptr)
		setContext(mWindow);

	//if glGetString is null glad was not initialized, or failed
	if (glGetString == nullptr) {
		if (gladLoadGL() == 0)//gladLoadGLLoader(GLADloadproc(glfwGetProcAddress)) == 0)//
		{
			mLogger.Printf("gladLoadGLLoader", "ShadersPlayer.cpp", "Failed to initialize GLAD", LOG_ERROR);
			return -1;
		}
		mLogger.Printf("gladLoadGLLoader", "ShadersPlayer.cpp", "GLAD successfully initialized", LOG_MESSAGE);
	}
	return 0;
}

bool ShadersPlayer::setVSync(const bool vsyncValue)
{
	mVSync = vsyncValue;
	//set to 1 for vsync
	glfwSwapInterval(mVSync);
	mLogger.Printf("setVSync", "ShadersPlayer.cpp", std::string(std::string("glfwSwapInterval set to ") + std::to_string(vsyncValue)).c_str(), LOG_MESSAGE);
	return mVSync;
}

float ShadersPlayer::setTimeMultiplier(const float timeMultiplier)
{
	return timeMultiplier < 0.0f ? timeMultiplier : mTimeMultiplier = timeMultiplier;
}

int32_t ShadersPlayer::windowShouldClose(int32_t status)
{
	if (status == 1 && mWindowShouldClose != 1) {
		mWindowShouldClose = bool(status);
		glfwSetWindowShouldClose(mWindow, mWindowShouldClose);
	}
	return mWindowShouldClose;
}

void ShadersPlayer::initBuffers()
{
	glGenVertexArrays(1, &mVAO);
	glGenBuffers(1, &mVBO);
	//glGenBuffers(1, &mEBO);

	glBindVertexArray(mVAO);

	glBindBuffer(GL_ARRAY_BUFFER, mVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(mVertices), mVertices, GL_STATIC_DRAW);

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEBO);
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	//EBO removed from msg
	mLogger.Printf("initBuffers", "ShadersPlayer.cpp", "VAO VBO  set up", LOG_MESSAGE);

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void ShadersPlayer::setError_callback(GLFWerrorfun callback) const
{
	if (callback == nullptr) {
		callback = error_callback;
		mLogger.Printf("setError_callback", "ShadersPlayer.cpp", "glfwSetErrorCallback callback invalid, setting up default", LOG_ERROR);
	}
	glfwSetErrorCallback(callback);
	mLogger.Printf("setError_callback", "ShadersPlayer.cpp", "glfwSetErrorCallback set", LOG_MESSAGE);
}

void ShadersPlayer::setKey_callback(GLFWkeyfun callback) const
{
	if (callback == nullptr) {
		callback = key_callback;
		mLogger.Printf("setKey_callback", "ShadersPlayer.cpp", "GLFWkeyfun callback invalid, setting up default", LOG_ERROR);
	}

	glfwSetKeyCallback(mWindow, callback);
	mLogger.Printf("setKey_callback", "ShadersPlayer.cpp", "glfwSetKeyCallback set", LOG_MESSAGE);
}

void ShadersPlayer::setFramebuffer_size_callback(GLFWframebuffersizefun callback) const
{
	if (callback == nullptr) {
		callback = framebuffer_size_callback;
		mLogger.Printf("setFramebuffer_size_callback", "ShadersPlayer.cpp", "glfwSetFramebufferSizeCallback callback invalid, setting up default", LOG_ERROR);
	}
	glfwSetFramebufferSizeCallback(mWindow, callback);
	mLogger.Printf("setFramebuffer_size_callback", "ShadersPlayer.cpp", "glfwSetFramebufferSizeCallback set", LOG_MESSAGE);
}

void ShadersPlayer::createShaderProgram(ShaderProgram& shader, const int32_t shaderRole, const uint32_t textWidth, const uint32_t textHeight)
{
	shader.mType = shaderRole;

	switch (shader.mType)
	{
	case SHADER_IMAGE:
		shader.mFHeader = mSHeaderImage;
		shader.mFMain = mDefaultFShaderImage;
		shader.mFFooter = mSFooterImage;

		shader.mVMain = mDefaultVShader;
		break;

	case SHADER_FBO:
		shader.mFHeader = mSHeaderFBO;
		shader.mFMain = mDefaultFShaderFBO;
		shader.mFFooter = mSFooterFBO;

		shader.mVMain = mDefaultVShader;
		break;

	case SHADER_SOUND:
		shader.mFHeader = mSHeaderSound;
		shader.mFMain = mDefaultFShaderSound;
		shader.mFFooter = mSFooterSound;

		shader.mVMain = mDefaultVShader;
		break;

	default:
		break;
	}
	mLogger.Printf("createShaderProgram", "ShadersPlayer.cpp", "ShaderProgram initialized", LOG_MESSAGE);
}

void ShadersPlayer::fillCommonFromFile(const std::string& path)
{
	std::ifstream file(path);
	const std::string src((std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>());

	if (src.empty())
		mLogger.Printf("fillCommonFromFile", "ShadersPlayer.cpp", "Common shader file empty, will set common shader empty", LOG_ERROR);

	fillCommon(src);
}

void ShadersPlayer::fillCommon(const std::string& shaderSrc)
{
	mCommonShader = shaderSrc;
	mLogger.Printf("fillCommon", "ShadersPlayer.cpp", "Common variables and funcs for all shaders set", LOG_ERROR);
}

void ShadersPlayer::fillShaderFromFile(ShaderProgram& shader, const std::string& path, const int32_t glFlag)
{
	std::ifstream file(path);
	const std::string src((std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>());

	if (src.empty())
		mLogger.Printf("fillShaderFromFile", "ShadersPlayer.cpp", "Shader file empty, will set default shader for this flag", LOG_ERROR);

	fillShader(shader, src, glFlag);
}

void ShadersPlayer::fillShader(ShaderProgram& shader, const std::string& shaderSrc, const int32_t glFlag)
{
	if (shaderSrc.empty())
	{
		mLogger.Printf("fillShader", "ShadersPlayer.cpp", "Setting default shader for this flag", LOG_ERROR);
		if (glFlag == GL_FRAGMENT_SHADER) {
			if (shader.mType == SHADER_IMAGE)
				shader.mFMain = mDefaultFShaderImage;
			else if (shader.mType == SHADER_FBO)
				shader.mFMain = mDefaultFShaderFBO;
			else if (shader.mType == SHADER_SOUND)
				shader.mFMain = mDefaultFShaderSound;
		}
		else if (glFlag == GL_VERTEX_SHADER) {
			shader.mVMain = mDefaultVShader;
		}
		return;
	}
	if (glFlag == GL_FRAGMENT_SHADER)
		shader.mFMain = shaderSrc;
	else if (glFlag == GL_VERTEX_SHADER)
		shader.mFMain = shaderSrc;

	mLogger.Printf("fillShader", "ShadersPlayer.cpp", "Shader set for this flag", LOG_ERROR);
}

void ShadersPlayer::setChannels(ShaderProgram& shader, const int32_t c0Type, const int32_t c1Type, const int32_t c2Type, const int32_t c3Type)
{
	shader.mFChannels = "";
	shader.mChannel[0].mChannelType = c0Type;
	shader.mChannel[1].mChannelType = c1Type;
	shader.mChannel[2].mChannelType = c2Type;
	shader.mChannel[3].mChannelType = c3Type;

	uint32_t id = 0;
	for (auto& channel : shader.mChannel)
	{
		shader.mFChannels += "uniform " + mChannelsLUT[channel.mChannelType] + " iChannel" + std::to_string(id++) + ";\n";
	}
}

int32_t ShadersPlayer::compileShaderProgram(ShaderProgram& shader)
{
	int32_t  success = 0;
	char infoLog[512]{};

	std::string shaderSrc = "";

	if (shader.mFChannels.empty())
		setChannels(shader, CHANNEL_SAMPLER_2D, CHANNEL_SAMPLER_2D, CHANNEL_SAMPLER_2D);

	//fragment shader
	shaderSrc = shader.mFHeader + shader.mFChannels + shader.mFMain + shader.mFFooter;
	const char* fragmentShader = shaderSrc.c_str();// std::cout << fragmentShader << "\n";
	shader.mFShaderId = glCreateShader(GL_FRAGMENT_SHADER);
	mLogger.Printf("compileShaderProgram", "ShadersPlayer.cpp", "Compiling a fragment shader", LOG_MESSAGE);

	glShaderSource(shader.mFShaderId, 1, &fragmentShader, nullptr);
	glCompileShader(shader.mFShaderId);

	//error checking
	glGetShaderiv(shader.mFShaderId, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(shader.mFShaderId, 512, nullptr, infoLog);
		mLogger.Printf("compileShaderProgram", "ShadersPlayer.cpp", infoLog, LOG_ERROR);
		return -1;
	}

	//vertex shader
	shaderSrc = shader.mVMain;
	const char* vertexShader = shaderSrc.c_str();// std::cout << vertexShader << "\n";
	shader.mVShaderId = glCreateShader(GL_VERTEX_SHADER);
	mLogger.Printf("compileShaderProgram", "ShadersPlayer.cpp", "Compiling a vertex shader", LOG_MESSAGE);

	glShaderSource(shader.mVShaderId, 1, &vertexShader, nullptr);
	glCompileShader(shader.mVShaderId);

	//error checking
	glGetShaderiv(shader.mVShaderId, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(shader.mVShaderId, 512, nullptr, infoLog);
		mLogger.Printf("compileShaderProgram", "ShadersPlayer.cpp", infoLog, LOG_ERROR);
		return -1;
	}

	mLogger.Printf("compileShaderProgram", "ShaderPlayer.cpp", "Compile successful", LOG_MESSAGE);
	return 0;
}

int32_t ShadersPlayer::linkShaders(ShaderProgram &shader, const bool isFeedBackLoop)
{
	int32_t  success(0);
	char infoLog[512];

	if (shader.mVShaderId == -1)
	{
		mLogger.Printf("linkShaders", "ShaderPlayer.cpp", "No vertex shader compiled", LOG_ERROR);
		compileShaderProgram(shader);
	}

	if (shader.mFShaderId == -1)
	{
		mLogger.Printf("linkShaders", "ShaderPlayer.cpp", "No fragment shader compiled", LOG_ERROR);
		compileShaderProgram(shader);
	}

	if (shader.mProgram == -1)
		shader.mProgram = glCreateProgram();
	else
	{
		deleteProgram(shader);
		shader.mProgram = glCreateProgram();
	}

	glAttachShader(shader.mProgram, shader.mVShaderId);
	//for (int32_t shader : mFragmentShaders)
	glAttachShader(shader.mProgram, shader.mFShaderId);

	glLinkProgram(shader.mProgram);

	glGetProgramiv(shader.mProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shader.mProgram, 512, nullptr, infoLog);
		mLogger.Printf("linkShaders", "ShadersPlayer.cpp", infoLog, LOG_ERROR);
		return -1;
	}
	mLogger.Printf("linkShaders", "ShadersPlayer.cpp", "Shader linking successful", LOG_MESSAGE);

	//mLogger.Printf("linkShaders", "ShadersPlayer.cpp", "Deleting Shaders", LOG_MESSAGE);
	//deleteShaders(shader);

	if (shader.mType == SHADER_FBO) {
		delete shader.mFBO;

		shader.mFBO = new FBOhandler(&mLogger, isFeedBackLoop);
		shader.mFBO->GenerateFBO(mWinWidth, mWinHeight);
	}

	glBindVertexArray(mVAO);
	glBindBuffer(GL_ARRAY_BUFFER, mVBO);

	//tell opengl how you read the VBO and pass args to the shaders
	//const uint32_t vPos = glGetAttribLocation(shader.mProgram, "vPos");
	// layout 0 for vPos
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)nullptr);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	return 0;
}

void ShadersPlayer::deleteShaders(ShaderProgram& shader)
{
	glDeleteShader(shader.mVShaderId);
	//for (auto shader : mFragmentShaders)
	glDeleteShader(shader.mFShaderId);
	shader.mVShaderId = -1;
	shader.mFShaderId = -1;

	mLogger.Printf("deleteShaders", "ShadersPlayer.cpp", "Shaders deleted", LOG_MESSAGE);
}

void ShadersPlayer::deleteProgram(ShaderProgram& shader)
{
	glDeleteProgram(shader.mProgram);
	shader.mProgram = -1;

	mLogger.Printf("deleteProgram", "ShadersPlayer.cpp", "Shader program deleted", LOG_MESSAGE);
}

void ShadersPlayer::deleteShaderProgram(ShaderProgram& shader)
{
	deleteShaders(shader);
	deleteProgram(shader);
	if (shader.mType == SHADER_FBO || shader.mType == SHADER_SOUND) {
		delete shader.mFBO;
		mLogger.Printf("deleteShaderProgram", "ShadersPlayer.cpp", "FBO and textures deleted", LOG_MESSAGE);
	}
}

void ShadersPlayer::addFBO(ShaderProgram& shader)
{
	if (shader.mType != SHADER_FBO)
	{
		mLogger.Printf("addFBO", "ShadersPlayer.cpp", "FBO is not of the right type or you can't add fbo anymore", LOG_ERROR);
		return;
	}
	mFBOs.push_back(shader);
	mLogger.Printf("addFBO", "ShadersPlayer.cpp", "FBO added you render queue", LOG_MESSAGE);
}

void ShadersPlayer::attachFBOToShader(ShaderProgram& shader, const int32_t channelId, ShaderProgram& FBO)
{
	if (FBO.mType != SHADER_FBO)
	{
		mLogger.Printf("attachFBOToShader", "ShadersPlayer.cpp", "FBO is not of the right type", LOG_ERROR);
		return;
	}

	shader.mChannel[channelId].mColorTexture = FBO.mFBO->getColorTexture();
	if (FBO.mFBO->isFeedBackLoop()) {
		shader.mChannel[channelId].myFbo = FBO.mFBO;
		mLogger.Printf("attachFBOToShader", "ShadersPlayer.cpp", "FBO (FeedBackLoop attached successfully", LOG_MESSAGE);
	}
	else
		mLogger.Printf("attachFBOToShader", "ShadersPlayer.cpp", "FBO attached successfully", LOG_MESSAGE);
}

void ShadersPlayer::setMainImageShaderProgram(const ShaderProgram& shader)
{
	if (shader.mType != SHADER_IMAGE)
	{
		mLogger.Printf("setMainImageShaderProgram", "ShaderPlayer.cpp", "Shader is not of type SHADER_IMAGE", LOG_ERROR);
		return;
	}
	mMainImage = shader;
	mLogger.Printf("setMainImageShaderProgram", "ShaderPlayer.cpp", "mMainImage set", LOG_MESSAGE);
}

void ShadersPlayer::swapMainImageShaderProgram(const ShaderProgram& shader)
{
	if (shader.mType != SHADER_IMAGE)
	{
		mLogger.Printf("swapMainImageShaderProgram", "ShaderPlayer.cpp", "Shader is not of type SHADER_IMAGE", LOG_ERROR);
		return;
	}
	mQueuededShaderProgram = shader;
	mNeedToSwapShaderProgram = true;
	mLogger.Printf("swapMainImageShaderProgram", "ShaderPlayer.cpp", "mMainImage set", LOG_MESSAGE);
}

inline void ShadersPlayer::getUniformIds(ShaderProgram& shader)
{
	shader.mPacker.iResolutionId = glGetUniformLocation(shader.mProgram, "iResolution");
	shader.mPacker.iTimeId = glGetUniformLocation(shader.mProgram, "iTime");
	shader.mPacker.iGlobalTimeId = glGetUniformLocation(shader.mProgram, "iGlobalTime");//itime without decimal part
	shader.mPacker.iMouseId = glGetUniformLocation(shader.mProgram, "iMouse");
	shader.mPacker.iDateId = glGetUniformLocation(shader.mProgram, "iDate");
	shader.mPacker.iSampleRateId = glGetUniformLocation(shader.mProgram, "iSampleRate");
	shader.mPacker.iChannelResolutionId = glGetUniformLocation(shader.mProgram, "iChannelResolution");
	shader.mPacker.iChannelTime = glGetUniformLocation(shader.mProgram, "iChannelTime");
	shader.mPacker.ifFragCoordOffsetUniformId = glGetUniformLocation(shader.mProgram, "ifFragCoordOffsetUniform");

	shader.mPacker.iChannel[0][0] = glGetUniformLocation(shader.mProgram, "iChannel[0].resolution");
	shader.mPacker.iChannel[0][1] = glGetUniformLocation(shader.mProgram, "iChannel[0].time");
	shader.mPacker.iChannel[1][0] = glGetUniformLocation(shader.mProgram, "iChannel[1].resolution");
	shader.mPacker.iChannel[1][1] = glGetUniformLocation(shader.mProgram, "iChannel[1].time");
	shader.mPacker.iChannel[2][0] = glGetUniformLocation(shader.mProgram, "iChannel[2].resolution");
	shader.mPacker.iChannel[2][1] = glGetUniformLocation(shader.mProgram, "iChannel[2].time");
	shader.mPacker.iChannel[3][0] = glGetUniformLocation(shader.mProgram, "iChannel[3].resolution");
	shader.mPacker.iChannel[3][1] = glGetUniformLocation(shader.mProgram, "iChannel[3].time");

	shader.mPacker.iChannel0 = glGetUniformLocation(shader.mProgram, "iChannel0");
	shader.mPacker.iChannel1 = glGetUniformLocation(shader.mProgram, "iChannel1");
	shader.mPacker.iChannel2 = glGetUniformLocation(shader.mProgram, "iChannel2");
	shader.mPacker.iChannel3 = glGetUniformLocation(shader.mProgram, "iChannel3");

	shader.mPacker.iTimeDeltaId = glGetUniformLocation(shader.mProgram, "iTimeDelta");
	shader.mPacker.iFrameId = glGetUniformLocation(shader.mProgram, "iFrame");
	shader.mPacker.iFrameRateId = glGetUniformLocation(shader.mProgram, "iFrameRate");

	mLogger.Printf("getUniformIds", "ShadersPlayer.cpp", "Uniforms ids set for a shader", LOG_MESSAGE);
}

void ShadersPlayer::queryUniforms()
{
	//maybe use mWinWidth without glfwGetWindowSize, setting the value with th e call back
	glfwGetWindowSize(mWindow, &mWinWidth, &mWinHeight);
	mTime = mFrameTimeStart * mTimeMultiplier;
	if (glfwGetMouseButton(mWindow, GLFW_MOUSE_BUTTON_LEFT))
	{
		glfwGetCursorPos(mWindow, &mMouseX, &mMouseY);
		mMouseY = mWinHeight - mMouseY;
		if (mMouseXNoDrag == 0.0f && mMouseYNoDrag == 0.0f)
		{
			mMouseXNoDrag = mMouseX;
			mMouseYNoDrag = mMouseY;
		}
	}
	else
	{
		mMouseXNoDrag = 0.0f;
		mMouseYNoDrag = 0.0f;
	}

	const auto secondsSinceMidnight = (
		std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch()).count() - 1525989600000) / 1000.0f;
	auto t = time(nullptr);
	const auto timePtr = localtime(&t);
	mMils = secondsSinceMidnight;
	mDay = float(timePtr->tm_mday);
	mMon = float(timePtr->tm_mon);
	mYear = float(timePtr->tm_year + 1970);
}

inline void ShadersPlayer::fillUniforms(const ShaderProgram& shader) const
{
	/*
	"struct Channel {"
	"    vec3    resolution;"
	"    float   time;"
	"};"

	"uniform vec3      iResolution;"                  // viewport resolution (in pixels)
	"uniform float     iTime;"                        // shader playback time (in seconds)
	"uniform float     iGlobalTime;"                  // shader playback time (in seconds)
	"uniform vec4      iMouse;"                       // mouse pixel coords
	"uniform vec4      iDate;"                        // (year, month, day, time in seconds)

	"uniform float     iSampleRate;"                  // sound sample rate (i.e., 44100)
	"uniform vec3      iChannelResolution[4];"        // channel resolution (in pixels)
	"uniform float     iChannelTime[4];"              // channel playback time (in sec)
	"uniform vec2       ifFragCoordOffsetUniform;"     // used for tiled based hq rendering
	--"uniform Channel   iChannel[4];"
	iChannel0
	iChannel1
	iChannel2
	iChannel3

	"uniform float     iTimeDelta;"                   // render time (in seconds)
	"uniform int       iFrame;"                       // shader playback frame
	"uniform float     iFrameRate;"
	*/
	switch (shader.mType)
	{
	case SHADER_IMAGE:
	{
		glUniform3f(shader.mPacker.iResolutionId, float(mWinWidth), float(mWinHeight), 1.0f);
		glUniform1f(shader.mPacker.iTimeId, (mFrameTimeStart)* mTimeMultiplier);
		glUniform1f(shader.mPacker.iGlobalTimeId, float(int32_t(mFrameTimeStart* mTimeMultiplier)));

		glUniform4f(shader.mPacker.iMouseId, float(mMouseX), float(mMouseY), float(mMouseXNoDrag), float(mMouseYNoDrag));
		glUniform4f(shader.mPacker.iDateId, float(mYear), float(mMon), float(mDay), mMils);
		glUniform1f(shader.mPacker.iSampleRateId, shader.mSampleRate);

		//###### CHANNEL STUFF ######
		//old way of setting channel params
		glUniform3fv(shader.mPacker.iChannelResolutionId, 4, shader.mChannelResolutions);
		glUniform1fv(shader.mPacker.iChannelTime, 4, shader.mChannelTime);
		glUniform2f(shader.mPacker.ifFragCoordOffsetUniformId, 0.0f, 0.0f);// ?? what is this

																		  //new way of setting channel params
		glUniform3fv(shader.mPacker.iChannel[0][0], 3, shader.mChannel[0].mResolution);
		glUniform1f(shader.mPacker.iChannel[0][1], shader.mChannel[0].mTime);
		glUniform3fv(shader.mPacker.iChannel[1][0], 3, shader.mChannel[1].mResolution);
		glUniform1f(shader.mPacker.iChannel[1][1], shader.mChannel[1].mTime);
		glUniform3fv(shader.mPacker.iChannel[2][0], 3, shader.mChannel[2].mResolution);
		glUniform1f(shader.mPacker.iChannel[2][1], shader.mChannel[2].mTime);
		glUniform3fv(shader.mPacker.iChannel[3][0], 3, shader.mChannel[3].mResolution);
		glUniform1f(shader.mPacker.iChannel[3][1], shader.mChannel[3].mTime);

		//put shader texture unit here
		glUniform1i(shader.mPacker.iChannel0, 0);// shader.mChannel[0].mColorTexture);
		glUniform1i(shader.mPacker.iChannel1, 1);
		glUniform1i(shader.mPacker.iChannel2, 2);
		glUniform1i(shader.mPacker.iChannel3, 3);
		//###### CHANNEL STUFF ######

		glUniform1f(shader.mPacker.iTimeDeltaId, mFrameDelay);
		glUniform1i(shader.mPacker.iFrameId, mFramePlayback);
		glUniform1f(shader.mPacker.iFrameRateId, mTrueFrameRate);
	}
	break;

	case SHADER_FBO:
	{
		glUniform3f(shader.mPacker.iResolutionId, float(mWinWidth), float(mWinHeight), 1.0f);
		glUniform1f(shader.mPacker.iTimeId, (mFrameTimeStart)* mTimeMultiplier);
		glUniform1f(shader.mPacker.iGlobalTimeId, float(int32_t(mFrameTimeStart* mTimeMultiplier)));

		glUniform4f(shader.mPacker.iMouseId, float(mMouseX), float(mMouseY), float(mMouseXNoDrag), float(mMouseYNoDrag));
		glUniform4f(shader.mPacker.iDateId, float(mYear), float(mMon), float(mDay), mMils);
		glUniform1f(shader.mPacker.iSampleRateId, shader.mSampleRate);

		//###### CHANNEL STUFF ######
		//old way of setting channel params
		glUniform3fv(shader.mPacker.iChannelResolutionId, 4, shader.mChannelResolutions);
		glUniform1fv(shader.mPacker.iChannelTime, 4, shader.mChannelTime);
		glUniform2f(shader.mPacker.ifFragCoordOffsetUniformId, 0.0f, 0.0f);// ?? what is this

																		   //new way of setting channel params
		glUniform3fv(shader.mPacker.iChannel[0][0], 3, shader.mChannel[0].mResolution);
		glUniform1f(shader.mPacker.iChannel[0][1], shader.mChannel[0].mTime);
		glUniform3fv(shader.mPacker.iChannel[1][0], 3, shader.mChannel[1].mResolution);
		glUniform1f(shader.mPacker.iChannel[1][1], shader.mChannel[1].mTime);
		glUniform3fv(shader.mPacker.iChannel[2][0], 3, shader.mChannel[2].mResolution);
		glUniform1f(shader.mPacker.iChannel[2][1], shader.mChannel[2].mTime);
		glUniform3fv(shader.mPacker.iChannel[3][0], 3, shader.mChannel[3].mResolution);
		glUniform1f(shader.mPacker.iChannel[3][1], shader.mChannel[3].mTime);

		//put shader texture unit here
		glUniform1i(shader.mPacker.iChannel0, 0);// shader.mChannel[0].mColorTexture);
		glUniform1i(shader.mPacker.iChannel1, 1);
		glUniform1i(shader.mPacker.iChannel2, 2);
		glUniform1i(shader.mPacker.iChannel3, 3);
		//###### CHANNEL STUFF ######

		glUniform1f(shader.mPacker.iTimeDeltaId, mFrameDelay);
		glUniform1i(shader.mPacker.iFrameId, mFramePlayback);
		glUniform1f(shader.mPacker.iFrameRateId, mTrueFrameRate);
	}
	break;

	case SHADER_SOUND:
	{

		glUniform4f(shader.mPacker.iDateId, float(mYear), float(mMon), float(mDay), mMils);

		glUniform1f(shader.mPacker.iSampleRateId, shader.mSampleRate);
		glUniform3fv(shader.mPacker.iChannelResolutionId, 4, shader.mChannelResolutions);
		glUniform1fv(shader.mPacker.iChannelTime, 4, shader.mChannelTime);
		//glUniform1f(shader.mPacker.iBlockOffset, shader.mBlockOffset);

		//put shader texture unit here
		glUniform1i(shader.mPacker.iChannel0, 0);// shader.mChannel[0].mColorTexture);
		glUniform1i(shader.mPacker.iChannel1, 1);
		glUniform1i(shader.mPacker.iChannel2, 2);
		glUniform1i(shader.mPacker.iChannel3, 3);
	}
	break;

	default:
		break;
	}
}

void ShadersPlayer::draw()
{
	//set all uniforms ids for the main shader
	getUniformIds(mMainImage);

	for (auto& fbo : mFBOs)
	{
		getUniformIds(fbo);
	}

	//wire polygons or not //GL_LINE
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

	glViewport(0, 0, mWinWidth, mWinHeight);

	//reset time since start
	glfwSetTime(0.0);

	while (!glfwWindowShouldClose(mWindow))
	{
		mFrameTimeStart = float(glfwGetTime());
		//glfwWaitEvents();		
		glfwPollEvents();

		queryUniforms();

		if (mNeedToSwapShaderProgram)
		{
			compileShaderProgram(mQueuededShaderProgram);
			//if linked without errors do stuff, else keep current working shader
			if (linkShaders(mQueuededShaderProgram) == 0) {
				deleteShaderProgram(mMainImage);
				setMainImageShaderProgram(mQueuededShaderProgram);
				getUniformIds(mMainImage);
			}
			mNeedToSwapShaderProgram = false;
		}

		if (needFBOUpdate)
		{
			for (auto& fbo : mFBOs)
			{
				fbo.mFBO->resize(mWinWidth, mWinHeight);
			}
			glViewport(0, 0, mWinWidth, mWinHeight);
			needFBOUpdate = false;
		}

		//todo self feeding buffer ??
		for (auto& fbo : mFBOs)
		{
			fbo.mFBO->bind();//todo bind swap textures 
			glClear(GL_COLOR_BUFFER_BIT);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, fbo.mChannel[0].myFbo == nullptr ?
				fbo.mChannel[0].mColorTexture : fbo.mChannel[0].myFbo->getColorTexture());
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, fbo.mChannel[1].myFbo == nullptr ?
				fbo.mChannel[1].mColorTexture : fbo.mChannel[1].myFbo->getColorTexture());
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, fbo.mChannel[2].myFbo == nullptr ?
				fbo.mChannel[2].mColorTexture : fbo.mChannel[2].myFbo->getColorTexture());
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D, fbo.mChannel[3].myFbo == nullptr ?
				fbo.mChannel[3].mColorTexture : fbo.mChannel[3].myFbo->getColorTexture());

			glUseProgram(fbo.mProgram);
			fillUniforms(fbo);

			glBindVertexArray(mVAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);

			glFlush();

			fbo.mFBO->unbind();			
		}

		//draw main image
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClear(GL_COLOR_BUFFER_BIT);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mMainImage.mChannel[0].mColorTexture);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, mMainImage.mChannel[1].mColorTexture);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, mMainImage.mChannel[2].mColorTexture);
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, mMainImage.mChannel[3].mColorTexture);

		glUseProgram(mMainImage.mProgram);
		fillUniforms(mMainImage);

		glBindVertexArray(mVAO);
		glDrawArrays(GL_TRIANGLES, 0, 6);


		//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);


		/*####################*/
		glFlush();
		glfwSwapBuffers(mWindow);

		mFrameDelay = float(glfwGetTime()) - mFrameTimeStart;
		if (mFrameDelay < mFrameTime)
			std::this_thread::sleep_for(std::chrono::milliseconds(long long((mFrameTime - mFrameDelay)*1000.0)));
		mTrueFrameRate = 1.0f / (float(glfwGetTime()) - mFrameTimeStart) + 0.5f;
		mFramePlayback++;
	}

	/*###### SHUT DOWN ######*/

	mLogger.Printf("End", "ShadersPlayer.cpp", "Deleting buffers", LOG_MESSAGE);

	glDeleteBuffers(1, &mVBO);
	glDeleteVertexArrays(1, &mVAO);
	//	glDeleteBuffers(1, &mEBO);

	mLogger.Printf("End", "ShadersPlayer.cpp", "Destroying context's window and do glfwTerminate()", LOG_MESSAGE);
	glfwDestroyWindow(mWindow);
	mWindow = nullptr;
	mWindowShouldClose = true;
	glfwTerminate();
}

double ShadersPlayer::setFramerate(const double fps)
{
	return fps < 0 ? 1.0f / mFrameTime : mFrameTime = 1.0 / fps;
}

void ShadersPlayer::error_callback(const int32_t error, const char* description)
{
	mLogger.Printf(std::to_string(error).c_str(), "error_callback", description, LOG_ERROR);
}

void ShadersPlayer::key_callback(GLFWwindow* window, const int32_t key, const int32_t scancode, const int32_t action, int32_t mods)
{
	if (action == GLFW_PRESS)
		switch (key)
		{
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, true);
			break;
		case GLFW_KEY_T:
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			break;
		case GLFW_KEY_F:
		{
			int count;
			GLFWmonitor** monitors = glfwGetMonitors(&count);
			if (mIsFullScreen)
			{
				glfwSetWindowMonitor(window, nullptr, 1920 / 5, 720 / 4, 1280, 720, GLFW_DONT_CARE);
				mIsFullScreen = false;
			}
			else
			{
				//always select the last monitor			
				glfwSetWindowMonitor(window, monitors[count - 1], 0, 0, 1920, 1280, GLFW_DONT_CARE);
				mIsFullScreen = true;
			}
			break;
		}

		default:
			return;
		}

	if (action == GLFW_RELEASE)
		switch (key)
		{
		case GLFW_KEY_T:
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		default:
			return;
		}
}

void ShadersPlayer::framebuffer_size_callback(GLFWwindow* window, const int width, const int height)
{
	needFBOUpdate = true;
}
#ifndef shaderPlayerLOGGER
#define shaderPlayerLOGGER

#include <iostream>
#include <thread>
#include <string>

typedef struct shaderLogStartInfo
{
	uint32_t  free_memory_MB;
	uint32_t  total_memory_MB;
	int32_t     number_cpus;
	wchar_t processor[512];
	int32_t     mhz;
	wchar_t os[512];
	wchar_t date[512];
	wchar_t gpuVendor[64];
	wchar_t gpuModel[512];
	uint32_t  total_videomemory_MB;
	int32_t     mScreenResolution[2];
	int32_t     mIntegratedMultitouch;
	int32_t     mNumMonitors;
} shaderLogStartInfo;


#define SHADERPLAYER_LOG_TXT 1
//#define SHADERPLAYER_LOG_WEIRD 2 
//#define SHADERPLAYER_LOG_WEIRD 4
//#define SHADERPLAYER_LOG_WEIRD 8 
#define	SHADERPLAYER_LOGGER_COUNT 1

enum
{
	LOG_ERROR = 1,
	LOG_WARNING,
	LOG_MESSAGE,
	LOG_DEBUG,

	LOG_TYPECOUNT
};

class basicLog
{
public:
	virtual ~basicLog() = default;

	virtual bool Init(const char* path, const shaderLogStartInfo* info) = 0;
	virtual void End() = 0;
	virtual void Printf(int32_t messageId, uint64_t threadId, const char* file, const char* func, int32_t line, int32_t type, const char* str) = 0;
};



class shaderPlayerLogTxt : public basicLog
{
public:
	shaderPlayerLogTxt() = default;
	~shaderPlayerLogTxt() override = default;

	bool Init(const char* path, const shaderLogStartInfo* info) override;
	void End() override;
	void Printf(int32_t messageId, uint64_t threadId, const char* file, const char* func, int32_t line, int32_t type, const char* str) override;

private:
	FILE* mFilePath;
};

class shaderPlayerLogger
{
public:
	shaderPlayerLogger() = default;
	~shaderPlayerLogger() = default;

	bool Init(const char* path, int32_t loggers);
	void End();
	void Printf(const char* func, const char* fileName, const char* str, int32_t type = -1, int32_t line = -1);

private:
	int32_t mMessageCounter;
	int32_t mNumLoggers;
	basicLog* mLoggers[8];
};

#endif
#include "shaderPlayerLogger.h"
#include "ShadersPlayer.h"
#include <GLFW/glfw3.h>


static ShadersPlayer sp;
static ShaderProgram Image{};
static ShaderProgram FBO_A{};
static ShaderProgram FBO_B{};
static ShaderProgram FBO_C{};
static ShaderProgram FBO_D{};
void worker()
{
	sp.Init();

	sp.createWindow(1280, 720, "Shaders?");

	sp.initGlad();

	sp.initBuffers();

	sp.fillCommon("");

	sp.createShaderProgram(Image, SHADER_IMAGE);
	sp.fillShaderFromFile(Image, "Image.glsl", GL_FRAGMENT_SHADER);
	sp.setChannels(Image, CHANNEL_TEXTURE_FBO);
	sp.compileShaderProgram(Image);
	sp.linkShaders(Image);

	sp.createShaderProgram(FBO_A, SHADER_FBO);
	sp.fillShaderFromFile(FBO_A, "FBOA.glsl", GL_FRAGMENT_SHADER);
	sp.setChannels(FBO_A, CHANNEL_TEXTURE_FBO);
	sp.compileShaderProgram(FBO_A);
	sp.linkShaders(FBO_A, true);


	sp.createShaderProgram(FBO_B, SHADER_FBO);
	sp.fillShaderFromFile(FBO_B, "FBOB.glsl", GL_FRAGMENT_SHADER);
	sp.setChannels(FBO_B, CHANNEL_TEXTURE_FBO);
	sp.compileShaderProgram(FBO_B);
	sp.linkShaders(FBO_B);


	sp.attachFBOToShader(Image, 0, FBO_A);

	sp.attachFBOToShader(FBO_A, 0, FBO_A);


	sp.addFBO(FBO_A);
	//sp.addFBO(FBO_B);

	sp.setMainImageShaderProgram(Image);

	sp.setVSync(true);
	sp.setFramerate(100.0f);
	sp.setTimeMultiplier(1.0f);

	sp.draw();
}

int main(int argc, char* argv[])
{


	std::thread drawingThread(worker);

	//swap shaders JIT
	//sp.createShaderProgram(shader, SHADER_IMAGE); // not needed, just overwrite current shader
	/*
	sp.fillShaderFromFile(shader, "basic.glsl", GL_FRAGMENT_SHADER);
	sp.swapMainImageShaderProgram(shader);
	*/

	while (!sp.windowShouldClose())
	{
		Sleep(100);
	}

	drawingThread.join();

	//Sleep(10000);
	return 0;
}

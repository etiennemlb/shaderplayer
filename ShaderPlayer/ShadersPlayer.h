#ifndef _SHADERSPLAYER_
#define _SHADERSPLAYER_

#include "shaderPlayerLogger.h"
#include "FBOhandler.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <string>
#include <vector>
#include <fstream>
#include <streambuf>
#include <chrono>
#include <thread>

#define VERTEXNUMBER(x) (x*3)

enum channelType
{
	CHANNEL_SAMPLER_2D,// SAME THING AS A FBO	
	CHANNEL_TEXTURE_FBO = 0,
	CHANNEL_SAMPLER_3D,
	CHANNEL_SAMPLER_CUBE,
};

enum shaderRole
{
	SHADER_IMAGE,
	SHADER_FBO,
	SHADER_SOUND,
};

struct Channel
{
	float mResolution[3]{};
	float mTime = 0.0f;
	//needed for FBO
	int32_t mChannelType = CHANNEL_SAMPLER_2D;
	FBOhandler *myFbo = nullptr;

	int32_t mColorTexture = -1;//FBO texture or simple texture from BMP or other
};

struct uniformPacker {
	//uniforms ids
	int32_t iResolutionId;
	int32_t iTimeId;
	int32_t iGlobalTimeId;
	int32_t iMouseId;
	int32_t iDateId;
	int32_t iSampleRateId;

	int32_t iChannelResolutionId; //chader specific
	int32_t iChannelTime; //chader specific

	int32_t ifFragCoordOffsetUniformId;

	int32_t iChannel[4][2]; //chader specific
	int32_t iChannel0; //chader specific
	int32_t iChannel1; //chader specific
	int32_t iChannel2; //chader specific
	int32_t iChannel3; //chader specific

	int32_t iTimeDeltaId;
	int32_t iFrameId;
	int32_t iFrameRateId;
};

struct ShaderProgram
{
	int32_t mType = SHADER_IMAGE; //shader specific

	std::string mFHeader;
	std::string mFChannels;
	std::string mFMain; // shader specific or not
	std::string mFFooter;

	std::string mVMain; // shader specific or not

	int32_t mVShaderId = -1; //shader specific or not
	int32_t mFShaderId = -1; //shader specific

	int32_t mProgram = -1;//shader specific

	float mSampleRate = 41000.0f;

	Channel mChannel[4]{};//shader specific

	//if a shader is of type sound/FBO
	FBOhandler* mFBO = nullptr;

	float mChannelResolutions[3 * 4]{}; //shader specific
	float mChannelTime[4]{}; //shader specific

	uniformPacker mPacker{}; //shader specific
};

class ShadersPlayer
{
private:
	GLFWwindow * mWindow;
	uint32_t mVBO;
	uint32_t mVAO;
	//uint32_t mEBO;

	std::string mCommonShader;

	ShaderProgram mMainImage;
	std::vector<ShaderProgram> mFBOs;


	double mFrameTime;
	float mTimeMultiplier;


	double mMouseX, mMouseY, mMouseXNoDrag, mMouseYNoDrag;
	int32_t mWinWidth, mWinHeight;
	float mTime;
	float mMils, mDay, mMon, mYear;
	int32_t mFramePlayback;
	float mFrameTimeStart;
	float mFrameDelay;
	float mTrueFrameRate;

	bool mVSync;

	bool mNeedToSwapShaderProgram;
	ShaderProgram mQueuededShaderProgram;

	bool mWindowShouldClose;
	static  bool mIsFullScreen;
	static  bool needFBOUpdate;
	static shaderPlayerLogger mLogger;

public:
	//todo add texture support ???
	ShadersPlayer() : mWindow(nullptr), mVBO(0), mVAO(0), /*mEBO(0),*/ mMainImage{},
		mFrameTime(1.0 / 60.0), mTimeMultiplier(1.0f), mMouseX(0), mMouseY(0), mMouseXNoDrag(0),
		mMouseYNoDrag(0), mWinWidth(0), mWinHeight(0), mTime(0), mMils(0), mDay(0), mMon(0), mYear(0),
		mFramePlayback(0), mFrameTimeStart(0), mFrameDelay(0), mTrueFrameRate(0), mVSync(false),
		mNeedToSwapShaderProgram(false), mQueuededShaderProgram{}, mWindowShouldClose(false)
	{
		mLogger.Init("log.txt", SHADERPLAYER_LOG_TXT);
		mLogger.Printf("ShadersPlayer", "ShadersPlayer.cpp", "Logger initialized", LOG_MESSAGE);
	}

	~ShadersPlayer()
	{
		glfwTerminate();
		mLogger.End();
	}


	int32_t Init() const;

	int32_t createWindow(uint32_t winWidth, uint32_t winHeight, const char * winTitle,
		GLFWmonitor* momnitor = nullptr, GLFWwindow * share = nullptr);

	GLFWwindow * setContext(GLFWwindow * window = nullptr);

	int32_t initGlad();

	void initBuffers();

	void setError_callback(GLFWerrorfun callback) const;
	void setKey_callback(GLFWkeyfun callback) const;
	void setFramebuffer_size_callback(GLFWframebuffersizefun callback) const;

	void createShaderProgram(ShaderProgram& shader, int32_t shaderRole, uint32_t textWidth = 512, uint32_t textHeight = 512);

	void fillCommonFromFile(const std::string& path);
	void fillCommon(const std::string& shaderSrc);
	void fillShaderFromFile(ShaderProgram& shader, const std::string& path, int32_t glFlag);
	void fillShader(ShaderProgram& shader, const std::string& shaderSrc, int32_t glFlag);

	void setChannels(ShaderProgram& shader, int32_t c0Type = CHANNEL_SAMPLER_2D, int32_t c1Type = CHANNEL_SAMPLER_2D, int32_t c2Type = CHANNEL_SAMPLER_2D, int32_t c3Type = CHANNEL_SAMPLER_2D);

	int32_t compileShaderProgram(ShaderProgram &shader);
	int32_t linkShaders(ShaderProgram& shader, bool isFeedBackLoop = false);
	void deleteShaders(ShaderProgram& shader);
	void deleteProgram(ShaderProgram& shader);
	void deleteShaderProgram(ShaderProgram& shader);

	void addFBO(ShaderProgram& shader);
	void attachFBOToShader(ShaderProgram& shader, int32_t channelId, ShaderProgram& FBO);

	void setMainImageShaderProgram(const ShaderProgram& shader);
	void swapMainImageShaderProgram(const ShaderProgram& shader);

	void draw();

	double setFramerate(double fps);
	bool setVSync(bool vsyncValue);
	float setTimeMultiplier(float timeMultiplier);

	int32_t windowShouldClose(int32_t status = -1);

private:

	inline void getUniformIds(ShaderProgram& shader);
	inline void queryUniforms();
	inline void fillUniforms(const ShaderProgram& shader) const;

	static const float mVertices[VERTEXNUMBER(128)];

	static const char* mSHeaderImage;
	static const char* mSHeaderFBO;
	static const char* mSHeaderSound;
	static const char* mVertexSHeader;

	static const std::string mChannelsLUT[3];

	static const char* mDefaultVShader;

	static const char* mDefaultFShaderImage;
	static const char* mDefaultFShaderFBO;
	static const char* mDefaultFShaderSound;

	static const char* mSFooterImage;
	static const char* mSFooterFBO;
	static const char* mSFooterSound;

	//default call backs
	static void error_callback(int32_t error, const char* description);
	static void key_callback(GLFWwindow* window, int32_t key, int32_t scancode, int32_t action, int32_t mods);
	static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
};

#endif
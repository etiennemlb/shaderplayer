#include "shaderPlayerLogger.h"

static void printHeader(FILE *const fp, const shaderLogStartInfo *info)
{
	fwprintf(fp, L"===================================================\n");
	fwprintf(fp, L"date  : %s\n", info->date);
	fwprintf(fp, L"\n");
	fwprintf(fp, L"Memory: %d / %d Megabytes \n", info->free_memory_MB, info->total_memory_MB);
	fwprintf(fp, L"CPU   : %s\n", info->processor);
	fwprintf(fp, L"Units : %d\n", info->number_cpus);
	fwprintf(fp, L"Speed : %d Mhz\n", info->mhz);
	fwprintf(fp, L"OS    : %s\n", info->os);
	fwprintf(fp, L"GPU   : %s, %s\n", info->gpuVendor, info->gpuModel);
	fwprintf(fp, L"VRam  : %d Megabytes \n", info->total_videomemory_MB);
	fwprintf(fp, L"Screen: %d x %d\n", info->mScreenResolution[0], info->mScreenResolution[1]);
	fwprintf(fp, L"Multitouch Integrated: %d\n", info->mIntegratedMultitouch);
	fwprintf(fp, L"Monitors: %d\n", info->mNumMonitors);
	fwprintf(fp, L"===================================================\n");
	fflush(fp);
}

bool shaderPlayerLogTxt::Init(const char* path, const shaderLogStartInfo* info)
{
	//path
	mFilePath = fopen(path, "wt");
	if (!mFilePath)
		return false;

	printHeader(mFilePath, info);

	return true;
}

void shaderPlayerLogTxt::End()
{
	fclose(mFilePath);
}

void shaderPlayerLogTxt::Printf(const int32_t messageId, const uint64_t threadId, const char* file, const char* func, const int32_t line, const int32_t type, const char* str)
{
	if (!mFilePath) return;

	//todo switch to fwprintf() one day
	switch (type)
	{
	case LOG_ERROR:
		fprintf(mFilePath, "   %d\t:: ", messageId);
		fprintf(mFilePath, "[%llu]  %s::%s (%d) :\n\t   ", threadId, file, func, line);
		fprintf(mFilePath, str);
		fprintf(mFilePath, "\n");
		break;

	case LOG_WARNING:
		fprintf(mFilePath, "   %d\t:: ", messageId);
		fprintf(mFilePath, "[%llu]  %s::%s (%d) :\n\t   ", threadId, file, func, line);
		fprintf(mFilePath, str);
		fprintf(mFilePath, "\n");
		break;

	case LOG_MESSAGE:
		fprintf(mFilePath, "   %d\t:: ", messageId);
		fprintf(mFilePath, "[%llu]  %s::%s (%d) :\n\t   ", threadId, file, func, line);
		fprintf(mFilePath, str);
		fprintf(mFilePath, "\n");
		break;

	case LOG_DEBUG:
		fprintf(mFilePath, "   %d\t::\n\t   ", messageId);
		fprintf(mFilePath, str);
		fprintf(mFilePath, "\n");
		break;
	default:
		fprintf(mFilePath, "   %d\t:: none\n", messageId);
	}

	fflush(mFilePath);
}



bool shaderPlayerLogger::Init(const char* path, const int32_t loggers)
{
	shaderLogStartInfo info = {};
	//todo, fill shaderLogInfo

	for (int32_t i = 0; i < SHADERPLAYER_LOGGER_COUNT; ++i)
	{
		if ((loggers & (1 << i)) == 0)
			continue;

		basicLog *lo(nullptr);

		switch (i)
		{
		case 0:
			lo = new shaderPlayerLogTxt();
			break;
			/*
			case 1:
			lo = new shaderPlayerLogTxt();
			break;
			*/

		default:
			continue;
		}

		if (!lo->Init(path, &info))
			return false;

		mLoggers[mNumLoggers++] = lo;
	}
	return true;
}

void shaderPlayerLogger::End()
{	
	for (int32_t i = 0; i < mNumLoggers; ++i)
	{
		mLoggers[i]->End();		
		delete mLoggers[i];
	}
}

void shaderPlayerLogger::Printf(const char* func, const char * fileName, const char * str, const int32_t type, const int32_t line)
{
	if (mNumLoggers < 1)
		return;

	//hash is not the true thread id
	const uint64_t tId = std::hash<std::thread::id>{}(std::this_thread::get_id());

	std::cout << "   " << mMessageCounter << "\t:: [" << tId << "] " <<
		fileName << "::" << func << " :\n\t    " << str << std::endl;

	for (int32_t i = 0; i < mNumLoggers; ++i)
	{
		/*//ugly but true windows thread id (not portable thought ?)
		std::stringstream ss;
		ss << std::this_thread::get_id();
		const int32_t id = std::stoull(ss.str());
		*/
		mLoggers[i]->Printf(mMessageCounter, tId, fileName, func, line, type, str);
	}

	++mMessageCounter;
}

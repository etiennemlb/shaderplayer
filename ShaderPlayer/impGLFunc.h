#ifndef CLASSICIMPORT
#define CLASSICIMPORT

#include <GLFW/glfw3.h>
#include <windows.h>

// define the function's prototype
typedef void(*GL_DELETESHADER) (GLuint shader);
#define glDeleteShader glad_glDeleteShader



GL_DELETESHADER glad_glDeleteShader;



void load() {
	// find the function and assign it to a function pointer
	glad_glDeleteShader = (GL_DELETESHADER)wglGetProcAddress("glGenBuffers");
}


#endif
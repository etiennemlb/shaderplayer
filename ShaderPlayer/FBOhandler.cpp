#include "FBOhandler.h"


int32_t FBOhandler::GenerateFBO(const uint32_t width, const uint32_t height)
{

	//Generate a framebuffer object(FBO) and bind it to the pipeline
	glGenFramebuffers(1, &mFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, mFBO);

	GenerateColorTexture(mTexture_color0, width, height);//generate empty texture
	if (mIsFeedBackLoop)
		GenerateColorTexture(mTexture_color1, width, height);//generate empty texture
	//GenerateDepthTexture(width, height);//generate empty texture


	//to keep track of our textures
	const unsigned int attachment_index_color_texture = 0;

	//bind textures to pipeline. texture_depth is optional
	//0 is the mipmap level. 0 is the heightest
	//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachment_index_color_texture, mTexture_color, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mTexture_color0, 0);
	if (mIsFeedBackLoop)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, mTexture_color1, 0);
	//glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, mTexture_depth, 0);//optional

	mDrawbuffer[0] = { GL_COLOR_ATTACHMENT0 };
	if (mIsFeedBackLoop)
		mDrawbuffer[1] = { GL_COLOR_ATTACHMENT1 };
	//where to save the rendered frame
	glDrawBuffers(1, &mDrawbuffer[0]);
	//Check for FBO completeness
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		mLogger->Printf("GenerateFBO", "FBOhandeler.cpp", "Failed to create a frame buffer", LOG_ERROR);
		return -1;
	}

	//unbind framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	mLogger->Printf("GenerateFBO", "FBOhandeler.cpp", "FBO created successfully", LOG_MESSAGE);
	return 0;
}

uint32_t FBOhandler::getColorTexture()
{
	//todo return a previously selected current texture if feedbackloop is activated
	return currentTexture = (currentTexture == mTexture_color0) && mIsFeedBackLoop ?
		mTexture_color1 : mTexture_color0;
}

bool FBOhandler::isFeedBackLoop()
{
	return mIsFeedBackLoop;
}

/*
uint32_t FBOhandler::getDepthTexture() const
{
	return mTexture_depth;
}
*/

void FBOhandler::resize(const uint32_t width, const uint32_t height)
{
	destroy();
	GenerateFBO(width, height);
}

void FBOhandler::bind() const
{
	//todo with feedbackloop is set switchbetween texture (first pass : tex1, snd pass text2 then text1...
	glBindFramebuffer(GL_FRAMEBUFFER, mFBO);
	//choose where to render
	if (mIsFeedBackLoop)
		glDrawBuffers(1, &mDrawbuffer[currentTexture == mTexture_color0 ? 0 : 1]);
	else
		glDrawBuffers(1, &mDrawbuffer[0]);
}

void FBOhandler::unbind() const
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FBOhandler::destroy() const
{
	glDeleteFramebuffers(1, &mFBO);
	glDeleteTextures(1, &mTexture_color0);
	glDeleteTextures(1, &mTexture_color1);
	//glDeleteTextures(1, &mTexture_depth);
	mLogger->Printf("destroy", "FBOhandeler.cpp", "FBO destroyed", LOG_ERROR);
}

void FBOhandler::GenerateColorTexture(uint32_t &texture, const uint32_t width, const uint32_t height) const
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	//create empty image
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, 0);
	//less memory intensive texture format
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16, width, height, 0, GL_RGBA, GL_INT, nullptr);

	//todo select params ???? during framebuffer creation? usefull ?
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
}
/*
void FBOhandler::GenerateDepthTexture(const uint32_t width, const uint32_t height)
{
	glGenTextures(1, &mTexture_depth);
	glBindTexture(GL_TEXTURE_2D, mTexture_depth);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
}
*/